package me.luanhenriquer8.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author luanhenriquer8
 */
@Entity
@Table(name = "contacts")
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "Contacts.findAll", query = "SELECT c FROM Contacts c")
        , @NamedQuery(name = "Contacts.findByContactId", query = "SELECT c FROM Contacts c WHERE c.contactId = :contactId")
        , @NamedQuery(name = "Contacts.findByContactNumber", query = "SELECT c FROM Contacts c WHERE c.contactNumber = :contactNumber")})
public class Contacts implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "contact_id")
    private Integer contactId;
    @Column(name = "contact_number")
    private Integer contactNumber;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "contactsFk")
    private List<Persons> personsList;
    @JoinColumn(name = "type_contact_fk", referencedColumnName = "contact_type_id")
    @ManyToOne(optional = false)
    private ContactType typeContactFk;

    public Contacts() {
    }

    public Contacts(Integer contactId) {
        this.contactId = contactId;
    }

    public Integer getContactId() {
        return contactId;
    }

    public void setContactId(Integer contactId) {
        this.contactId = contactId;
    }

    public Integer getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(Integer contactNumber) {
        this.contactNumber = contactNumber;
    }

    @XmlTransient
    public List<Persons> getPersonsList() {
        return personsList;
    }

    public void setPersonsList(List<Persons> personsList) {
        this.personsList = personsList;
    }

    public ContactType getTypeContactFk() {
        return typeContactFk;
    }

    public void setTypeContactFk(ContactType typeContactFk) {
        this.typeContactFk = typeContactFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (contactId != null ? contactId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Contacts)) {
            return false;
        }
        Contacts other = (Contacts) object;
        if ((this.contactId == null && other.contactId != null) || (this.contactId != null && !this.contactId.equals(other.contactId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Pacote.Contacts[ contactId=" + contactId + " ]";
    }

}
