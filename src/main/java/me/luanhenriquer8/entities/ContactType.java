package me.luanhenriquer8.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author luanhenriquer8
 */
@Entity
@Table(name = "contact_type")
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "ContactType.findAll", query = "SELECT c FROM ContactType c")
        , @NamedQuery(name = "ContactType.findByContactTypeId", query = "SELECT c FROM ContactType c WHERE c.contactTypeId = :contactTypeId")
        , @NamedQuery(name = "ContactType.findByContactTypeDescription", query = "SELECT c FROM ContactType c WHERE c.contactTypeDescription = :contactTypeDescription")})
public class ContactType implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "contact_type_id")
    private Integer contactTypeId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "contact_type_description")
    private String contactTypeDescription;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "typeContactFk")
    private List<Contacts> contactsList;

    public ContactType() {
    }

    public ContactType(Integer contactTypeId) {
        this.contactTypeId = contactTypeId;
    }

    public ContactType(Integer contactTypeId, String contactTypeDescription) {
        this.contactTypeId = contactTypeId;
        this.contactTypeDescription = contactTypeDescription;
    }

    public Integer getContactTypeId() {
        return contactTypeId;
    }

    public void setContactTypeId(Integer contactTypeId) {
        this.contactTypeId = contactTypeId;
    }

    public String getContactTypeDescription() {
        return contactTypeDescription;
    }

    public void setContactTypeDescription(String contactTypeDescription) {
        this.contactTypeDescription = contactTypeDescription;
    }

    @XmlTransient
    public List<Contacts> getContactsList() {
        return contactsList;
    }

    public void setContactsList(List<Contacts> contactsList) {
        this.contactsList = contactsList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (contactTypeId != null ? contactTypeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContactType)) {
            return false;
        }
        ContactType other = (ContactType) object;
        if ((this.contactTypeId == null && other.contactTypeId != null) || (this.contactTypeId != null && !this.contactTypeId.equals(other.contactTypeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Pacote.ContactType[ contactTypeId=" + contactTypeId + " ]";
    }

}
