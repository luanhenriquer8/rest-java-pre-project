package me.luanhenriquer8.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author luanhenriquer8
 */
@Entity
@Table(name = "persons")
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "Persons.findAll", query = "SELECT p FROM Persons p")
        , @NamedQuery(name = "Persons.findByPersonId", query = "SELECT p FROM Persons p WHERE p.personId = :personId")
        , @NamedQuery(name = "Persons.findByPersonName", query = "SELECT p FROM Persons p WHERE p.personName = :personName")})
public class Persons implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "person_id")
    private Integer personId;
    @Size(max = 45)
    @Column(name = "person_name")
    private String personName;
    @JoinColumn(name = "contacts_fk", referencedColumnName = "contact_id")
    @ManyToOne(optional = false)
    private Contacts contactsFk;

    public Persons() {
    }

    public Persons(Integer personId) {
        this.personId = personId;
    }

    public Integer getPersonId() {
        return personId;
    }

    public void setPersonId(Integer personId) {
        this.personId = personId;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public Contacts getContactsFk() {
        return contactsFk;
    }

    public void setContactsFk(Contacts contactsFk) {
        this.contactsFk = contactsFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (personId != null ? personId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Persons)) {
            return false;
        }
        Persons other = (Persons) object;
        if ((this.personId == null && other.personId != null) || (this.personId != null && !this.personId.equals(other.personId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Pacote.Persons[ personId=" + personId + " ]";
    }

}

